#!/usr/bin/env sh
build_dir="./build_pip/"
pkg_dir="auroradns"

rm -rf ${build_dir}
mkdir -p ${build_dir}

rsync -avP * --exclude=requirements.txt --exclude=build_pip.sh --exclude-from=.gitignore --exclude=setup.py --exclude=LICENSE --exclude=README.md ${build_dir}/${pkg_dir}
rsync -avP setup.py LICENSE README.md ${build_dir}/

sed -i 's/from common/from auroradns.common/g' ${build_dir}/auroradns/cli.py

cd ${build_dir}

python3 setup.py sdist bdist_wheel

if [ "${1}" == "publish" ]; then
  echo "Publishing to pypi.org"
  read -p "Press enter to continue..."
  python3 -m twine upload dist/*
else
  echo "Using test.pypi.org, use \"${0} publish\" to deploy to pypi.org"
  read -p "Press enter to continue..."
  python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*
fi
